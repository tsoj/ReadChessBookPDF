# ReadChessBookPDF

## Setup

```shell
conda create -n read_chess_books_env python=3.9
conda activate read_chess_books_env

pip install pymupdf
pip install torch torchvision # --index-url https://download.pytorch.org/whl/rocm5.7

git submodule update --init
pip install -e ./Chess_diagram_to_FEN/
```