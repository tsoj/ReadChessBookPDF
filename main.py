import fitz
import io
import os
import sys
from pathlib import Path
from PIL import Image
import chess

from Chess_diagram_to_FEN.chess_diagram_to_fen import get_fen

from blacklist import blacklist


def board_str_repr(fen: str, fancy=False) -> str:
    board = chess.Board(fen)

    if fancy:
        # create an empty string to store the result
        result = "\n Begin board (\n"
        # loop through the rows of the board from 8 to 1
        for row in range(8, 0, -1):
            # loop through the columns of the board from a to h
            for col in range(8):
                # get the square name, such as a1, b1, etc.
                square = chess.square(col, row - 1)
                # get the piece on that square, or None if empty
                piece = board.piece_at(square)
                # if there is a piece, append its color and symbol, such as w R, b N, etc.
                if piece:
                    color = "w" if piece.color else "b"
                    symbol = piece.symbol().upper()
                    result += f"{square}: {color} {symbol} "
                # otherwise, append an empty placeholder, such as a1: __, b1: __, etc.
                else:
                    result += f"{square}: ___ "
            # append a newline after each row
            result += "\n"
        # return the result strin
        result += ") End board. \n"
        return result
    else:
        return " (FEN: " + board.fen().split().pop(0) + ") "


def to_text(in_file_name, out_dir_root, failure_out_dir_root):

    print("Converting to text:", in_file_name)

    assert failure_out_dir_root != out_dir_root, "Failures should be stored seperately"

    stem_file_name = Path(in_file_name).stem
    out_dir = out_dir_root + stem_file_name + "_result/"
    failure_out_dir = failure_out_dir_root + stem_file_name + "_result/"

    if Path(out_dir).exists():
        print("Skipping, out_dir already exists:", out_dir)
        return
    if Path(failure_out_dir).exists():
        print("Skipping, failure_out_dir already exists:", failure_out_dir)
        return

    allow_auto_rotate = False
    do_mirror_on_180 = False

    if str(stem_file_name) in blacklist:
        if blacklist[str(stem_file_name)] == "skip":
            print("Skipping because", stem_file_name, "is on the blacklist")
            return
        if blacklist[str(stem_file_name)] == "allow_rotation":
            allow_auto_rotate = True
        if blacklist[str(stem_file_name)] == "allow_rotation_and_mirror":
            allow_auto_rotate = True
            do_mirror_on_180 = True

    Path(out_dir_root).mkdir(exist_ok=True)
    Path(failure_out_dir_root).mkdir(exist_ok=True)

    os.mkdir(out_dir)

    pdf_file = fitz.open(in_file_name)

    image_dir = out_dir + "image_folder"
    os.mkdir(image_dir)

    text = ""
    image_number = 0

    success = True

    num_images = 0
    num_images_with_fen = 0

    # iterate over PDF pages
    for page in pdf_file:
        # extract the text and images from the page as a dictionary
        page_dict = page.get_text("dict")
        for block in page_dict["blocks"]:
            # check if the block is an image
            if block["type"] == 1:
                # load it to PIL
                image = Image.open(io.BytesIO(block["image"]))
                image_ext = image.format.lower()
                num_images += 1
                image_number += 1

                if (
                    str(stem_file_name) in blacklist
                    and blacklist[str(stem_file_name)] is list[int]
                    and image_number in blacklist[str(stem_file_name)]
                ):
                    print(
                        "Skipping because image number",
                        image_number,
                        "for",
                        stem_file_name,
                        "is on the blacklist",
                    )
                    continue

                fen_result = get_fen(image, mirror_when_180_rotation=do_mirror_on_180)
                failed_reason = None
                fen = None

                if fen_result is None:
                    failed_reason = "No_chessboard"
                else:
                    fen = fen_result.fen

                    if fen is None:
                        failed_reason = "FEN_is_None"
                    else:
                        num_images_with_fen += 1

                if (
                    fen_result is not None
                    and fen is not None
                    and fen_result.image_rotation_angle != 0
                    and not allow_auto_rotate
                ):
                    print("Failure because of possibly rotated images")
                    success = False
                    if failed_reason is None:
                        failed_reason = "Rotated"

                if failed_reason is None:

                    num_images_with_fen += 1
                    text += board_str_repr(fen)

                    fake_fen = fen.replace("/", "_").replace(" ", "+")
                    image_filename = f"{image_dir}/image_{fake_fen}.{image_ext}"

                    if fen_result.cropped_image:
                        fen_result.cropped_image.save(open(image_filename, "wb"))
                    else:
                        failed_reason = "Not_cropped"
                        success = False

                    print(fen)

                if failed_reason is not None:
                    failed_image_filename = f"{image_dir}/000_FAILED_{failed_reason}_image_{str(image_number)}.{image_ext}"
                    image.save(open(failed_image_filename, "wb"))

                image_number += 1

            # check if the block is text
            elif block["type"] == 0:
                # loop through the lines in the block
                for line in block["lines"]:
                    # loop through the spans in the line
                    for span in line["spans"]:
                        # write the span text to the text file
                        text += span["text"] + "\n"

    if num_images_with_fen * 2 <= num_images:
        print("Failure because of too few successful images")
        success = False

    if num_images <= 10:
        print("Failure because of too few images")
        success = False

    text = text.replace("♔", "K")
    text = text.replace("♚", "K")
    text = text.replace("♕", "Q")
    text = text.replace("♛", "Q")
    text = text.replace("♖", "R")
    text = text.replace("♜", "R")
    text = text.replace("♗", "B")
    text = text.replace("♝", "B")
    text = text.replace("♘", "N")
    text = text.replace("♞", "N")
    text = text.replace("♙", "P")
    text = text.replace("♟", "P")

    text = text.encode("ascii", "ignore").decode("ascii")
    text = text.replace("\n", " ")

    text = " ".join(text.split())

    out_file_name = out_dir + stem_file_name + ".txt"
    with os.fdopen(
        os.open(out_file_name, os.O_CREAT | os.O_WRONLY | os.O_EXCL), "w"
    ) as text_file:
        text_file.write(text)

    if not success:
        os.rename(out_dir, failure_out_dir)


assert (
    2 <= len(sys.argv) <= 3
), "Need the PDF file as argument and optionally an output dir"

in_file_name = Path(sys.argv[1])

out_root_dir = "./results/"
if len(sys.argv) == 3:
    out_root_dir = sys.argv[2] + "/"

failure_out_dir_root = out_root_dir + "failures/"


if Path.is_dir(in_file_name):
    pdf_list = list(in_file_name.glob("**/*.pdf"))
else:
    pdf_list = [in_file_name]


for pdf_file in pdf_list:
    to_text(
        in_file_name=pdf_file,
        out_dir_root=out_root_dir,
        failure_out_dir_root=failure_out_dir_root,
    )


# TODO https://github.com/linrock/chessboard-recognizer
